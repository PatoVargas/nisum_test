package com.example.NisumTest;

import com.example.NisumTest.Controller.PersonController;
import com.example.NisumTest.Domain.Person;
import com.example.NisumTest.RepositoryInterface.PersonRepositoryInterface;
import com.example.NisumTest.ServiceInterface.PersonServiceInterface;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.BDDMockito.given;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
        
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(PersonController.class)
public class NisumTestApplicationTests {
    
    @Autowired
    private MockMvc mvc;
 
    @MockBean
    private PersonServiceInterface personServiceInterface;
    
    @MockBean
    private PersonRepositoryInterface repositoryInterface;
    
    @Autowired
    private ObjectMapper objectMapper;
    
    @Test
    public void getAllReturnsOK() throws Exception {

        Person person = new Person();
        person.setName("Name 1");
        person.setLastname("Last Name 1");
        person.setAddress("Address 1");
        person.setPhonenumber(123456);
        person.setColourhair("black");

        List<Person> persons = Arrays.asList(person);

        given(personServiceInterface.getAll()).willReturn(persons);
        
        mvc.perform(get("/persons")
          .contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isOk());
    }
    
    @Test
    public void createReturnsOK() throws Exception {
        
        Person person = new Person();
        person.setName("Name 1");
        person.setLastname("Last Name 1");
        person.setAddress("Address 1");
        person.setPhonenumber(123456);
        person.setColourhair("black");

        mvc.perform(post("/persons")
            .contentType(MediaType.APPLICATION_JSON)
            .characterEncoding("utf-8")
            .content(objectMapper.writeValueAsString(person))
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
    }
    
    @Test
    public void createFailByHair() throws Exception {
        
        Person person = new Person();
        person.setName("Name 1");
        person.setLastname("Last Name 1");
        person.setAddress("Address 1");
        person.setPhonenumber(123456);
        person.setColourhair("amarillo");

        mvc.perform(post("/persons")
            .contentType(MediaType.APPLICATION_JSON)
            .characterEncoding("utf-8")
            .content(objectMapper.writeValueAsString(person))
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest());
    }
    
    @Test
    public void deleteFailById() throws Exception {

        mvc.perform(delete("/persons/{id}", 1000))
            .andExpect(status().isNotFound());
    }
    
}