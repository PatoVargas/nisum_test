package com.example.NisumTest.Errors;

/**
 *
 * @author patriciovargas
 */

public class NotFoundItem {
    
    private String errorMessage;
    
    public NotFoundItem(String errorMessage){
        this.errorMessage = errorMessage;
    }
 
    public String getErrorMessage() {
        return errorMessage;
    }
}
