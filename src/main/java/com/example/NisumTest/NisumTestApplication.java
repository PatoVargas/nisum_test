package com.example.NisumTest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com"})
public class NisumTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(NisumTestApplication.class, args);
	}

}
