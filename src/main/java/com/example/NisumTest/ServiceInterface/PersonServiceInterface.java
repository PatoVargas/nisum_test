package com.example.NisumTest.ServiceInterface;

import com.example.NisumTest.Domain.Person;
import java.util.List;

/**
 *
 * @author patriciovargas
 */

public interface PersonServiceInterface {
 
    public List<Person> getAll();
    
    public Person getOne(int id);
    
    public Person createPerson(Person person);
    
    public Person updatePerson(Person new_person, int id);
    
    public boolean deletePerson(int id);
}
