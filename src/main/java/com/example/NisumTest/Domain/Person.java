package com.example.NisumTest.Domain;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author patriciovargas
 */

@Entity
public class Person implements Serializable {    
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    
    private String name;
    
    private String lastname;
    
    private String address;
    
    private Integer phonenumber;
    
    private String colourhair;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(Integer phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getColourhair() {
        return colourhair;
    }

    public void setColourhair(String colourhair) {
        this.colourhair = colourhair;
    }

    public Person() {
    }

    public Person(Integer id, String name, String lastname, String address, Integer phonenumber, String colourhair) {
        this.id = id;
        this.name = name;
        this.lastname = lastname;
        this.address = address;
        this.phonenumber = phonenumber;
        this.colourhair = colourhair;
    }
    
    public Person updateAttributes(Person person){
        if (validateStringField(person.address)) {
            this.setAddress(person.address);
        }
        if (validateStringField(person.name)) {
            this.setName(person.name);
        }
        if (validateStringField(person.lastname)) {
            this.setLastname(person.lastname);
        }
        if (validateStringField(person.colourhair)) {
            this.setColourhair(person.colourhair);
        }
        if (validateNumberField(person.phonenumber)) {
            this.setPhonenumber(person.phonenumber);
        }
        return this;
    }
    
    private boolean validateStringField(String field){
        return field != null;
    }
    
    private boolean validateNumberField(Integer field){
        return field != null;
    }
    
    public boolean validateColour(){
        List<String> valided = Arrays.asList("black","red","yellow","brown","white");
        return valided.contains(this.getColourhair());
    }
}
