package com.example.NisumTest.RepositoryImplementation;

import com.example.NisumTest.RepositoryInterface.PersonRepositoryInterface;
import org.springframework.stereotype.Repository;

/**
 *
 * @author patriciovargas
 */

@Repository
public abstract class PersonRepositoryImpl implements PersonRepositoryInterface{
    
}
