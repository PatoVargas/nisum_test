package com.example.NisumTest.Controller;

import com.example.NisumTest.Domain.Person;
import com.example.NisumTest.Errors.NotFoundItem;
import com.example.NisumTest.ServiceInterface.PersonServiceInterface;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author patriciovargas
 */

@RestController
public class PersonController {
    
    @Autowired
    PersonServiceInterface personServiceInterface;
    
    @GetMapping(value = "/persons")
    public List<Person> getAll(){
        return personServiceInterface.getAll();
    }
    
    @GetMapping(value = "/persons/{id}")
    public ResponseEntity<?> getOneById(@PathVariable int id){
        Person person = personServiceInterface.getOne(id);
        if (person == null) {
            return new ResponseEntity(new NotFoundItem("Person with id " + id 
                    + " not found"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Person>(person, HttpStatus.OK);
    }
    
    @PostMapping(value="/persons")
    public ResponseEntity<?> createPerson(@RequestBody Person person){
        if (person.validateColour() || person.getColourhair() == null) {
            return new ResponseEntity<Person>(personServiceInterface.createPerson(person), HttpStatus.OK);
        }
        else{
            return new ResponseEntity(new NotFoundItem("The hair colour is not valid"), HttpStatus.BAD_REQUEST);
        }
    }
    
    @PutMapping(value = "/persons/{id}")
    public ResponseEntity<?> updatePerson(@RequestBody Person new_person, @PathVariable int id){
        if (new_person.validateColour() || new_person.getColourhair() == null) {
            Person person = personServiceInterface.updatePerson(new_person, id);
            if (person == null) {
                return new ResponseEntity(new NotFoundItem("Person with id " + id 
                        + " not found"), HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<Person>(person, HttpStatus.OK);
        }
        else{
            return new ResponseEntity(new NotFoundItem("The hair colour is not valid"), HttpStatus.BAD_REQUEST);
        }
    }
    
    @DeleteMapping(value = "/persons/{id}")
    public ResponseEntity deletePerson(@PathVariable int id){
        boolean response = personServiceInterface.deletePerson(id);
        if (response) {
            return new ResponseEntity(HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }
}
