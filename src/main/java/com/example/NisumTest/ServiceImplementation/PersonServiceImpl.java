package com.example.NisumTest.ServiceImplementation;

import com.example.NisumTest.Domain.Person;
import com.example.NisumTest.RepositoryInterface.PersonRepositoryInterface;
import com.example.NisumTest.ServiceInterface.PersonServiceInterface;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author patriciovargas
 */

@Service
public class PersonServiceImpl implements PersonServiceInterface{

    
    @Autowired
    PersonRepositoryInterface personRepositoryInterface;
    
    @Override
    public List<Person> getAll() {
        return personRepositoryInterface.findAll();
    }

    @Override
    public Person getOne(int id) {
        if (personRepositoryInterface.existsById(id)) {
            return personRepositoryInterface.getOne(id);
        }
        return null;
    }    

    @Override
    public Person createPerson(Person person) {
        return personRepositoryInterface.save(person);
    }

    @Override
    public Person updatePerson(Person new_person, int id) {
        if (personRepositoryInterface.existsById(id)) {
            Person person = personRepositoryInterface.getOne(id);
            person.updateAttributes(new_person);
            return personRepositoryInterface.save(person);
        }
        return null;
    }

    @Override
    public boolean deletePerson(int id) {
        if (personRepositoryInterface.existsById(id)) {
            personRepositoryInterface.deleteById(id);
            return true;
        }
        return false;
    }
    
}
