package com.example.NisumTest.RepositoryInterface;

import com.example.NisumTest.Domain.Person;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author patriciovargas
 */

public interface PersonRepositoryInterface extends JpaRepository<Person, Integer>{
    
}
