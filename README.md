# CRUD Project - NISUM
# Owner: Patricio Vargas

To run test in the root folder execute:

```sh
$ mvn clean test 
```

To rut project in the root folder execute:

```sh
$ mvn clean test spring-boot:run 
```
